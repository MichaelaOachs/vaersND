﻿# source 1
# r - Convert column classes in data.table - Stack Overflow answered by Matt Dowle on Dec 27 2013. See \url{http://stackoverflow.com/questions/7813578/convert-column-classes-in-data-table}.

# Data source \url{https://vaers.hhs.gov/data/index}


library(data.table)

# Vax
vaersND_vax <- fread("/vaers_data/raw/ND/NonDomesticVAERSVAX.csv", colClasses = "character")

# changing column to numeric class
change_class1 <- "VAX_DOSE"
for (col in change_class1) set(vaersND_vax, j = col, value = as.numeric(vaersND_vax[[col]])) # Source 1

save(vaersND_vax, file = "/data/vaersND_vax.RData", compress = "xz")


# Symptoms
vaersND_symptoms <- fread("/vaers_data/raw/ND/NonDomesticVAERSSYMPTOMS.csv", colClasses = "character")

change_class2 <- c("SYMPTOMVERSION1", "SYMPTOMVERSION2", "SYMPTOMVERSION3", "SYMPTOMVERSION4", "SYMPTOMVERSION5")
for (col in change_class2) set(vaersND_symptoms, j = col, value = as.numeric(vaersND_symptoms[[col]])) # Source 1

save(vaersND_symptoms, file = "/data/vaersND_symptoms.RData", compress = "xz")



# Data
vaersND_data <- fread("/vaers_data/raw/ND/NonDomesticVAERSDATA.csv", colClasses = "character")

change_class3 <- c("AGE_YRS", "CAGE_YR", "CAGE_MO", "HOSPDAYS", "NUMDAYS")
for (col in change_class3) set(vaersND_data, j = col, value = as.numeric(vaersND_data[[col]])) # Source 1

change_class4 <- c("RECVDATE", "RPT_DATE", "DATEDIED", "VAX_DATE", "ONSET_DATE")
for (col in change_class4) set(vaersND_data, j = col, value = as.POSIXct(vaersND_data[[col]], format = "%m/%d/%Y")) # Source 1

save(vaersND_data, file = "/data/vaersND_data.RData", compress = "xz")
